## Descrição

Este é um guia para um sistema de login simples utilizando AdonisJS (para criação da API e gestão da base de dados) + React (para as telas do usuário).

## Requisitos

Este guia considera que vocẽ já possui tanto o React quando o AdonisJS instalados em seu sistema e que possui os conhecimentos necessários para configurá-los e conectar à base de dados.

## Guia

### Tabelas

Vamos considerar a seguinte tabela em uma base de dados: Tabela `usuarios` com campos `id, nome, senha`.

Além disso, também criaremos a tabela `sessoes` com os campos `id, usuario_id, codigo, expiracao`.

### Utilizando o Lucid ORM

Crie um arquivo `app/Models/Usuario.js` com o seguinte código para utilizar como Model em sua aplicação, lembre-se de alterá-lo conforme seu próprio projeto, este é apenas um exemplo.

```
'use strict'

const Model = use('Model')

class Usuario extends Model {
  static get table () {
    return 'usuarios'
  }
}

module.exports = Usuario
```

Crie também um arquivo `app/Models/Sessao.js` com o seguinte código:

```
'use strict'

const Model = use('Model')

class Sessao extends Model {
  static get table () {
    return 'sessoes'
  }
}

module.exports = Sessao
```


### Criando um usuário

Para criar o usuáro, utilize o seguinte código em qualquer lugar que será executado, pode ser em um `controller` ou direto em uma de suas rotas.

Caso já tenha seu usuário cadastrado, pode pular essa etapa.

```
const Hash = use('Hash')
const Usuario = use('App/Models/Usuario')

// Instancia um novo usuário
const usuario = new Usuario()

// Define o nome de usuário
usuario.nome = 'admin'

// Define a senha do usuário, após aplicar uma camada de segurança sobre a mesma
usuario.senha = await Hash.make('admin-password')

// Salva o usuário na base de dados
await usuario.save()
```

### Consulta

Em seu arquivo de rotas `start/routes.js`, crie uma rota para o login do usuário e outra para a validação das sessões. O código utilizado no exemplo pode ser colocado dentro de um `Controller`, mas para conclusão rápida, criaremos diretamente na rota.

```
const Route = use('Route')
const Hash = use('Hash')
const Usuario = use('App/Models/Usuario')
const Sessao = use('App/Models/Sessao')

// A rota deve ser POST, pois será criada uma sessão
Route.post('api/login', async ({ request, response }) => {
  const { nome, senha } = request.all()

  const usuario = await Usuario.findBy('nome', nome);

  if(!usuario) {
    response.send({error: "Usuário não encontrado"});
  } else {
    const senhaCorreta = await Hash.verify(senha, usuario.senha)

    if(!senhaCorreta) {
        response.send({error: "Senha incorreta"});
    } else {
        // Cria a sessão do usuario
        const sessao = new Sessao()

        // Cria informações únicas para gerar um código de sessao, utilizamos aspas duplas entre as concatenações para evitar somas
        const date = new Date();
        const cod = "" + date.getFullYear() + "" + date.getMonth() + "" + date.getDate() + "" + date.getHours() + "" + date.getMinutes() + "" + date.getSeconds();

        // Cria um Hash com o nome do usuario para que o código seja ainda mais único

        let hashed = await Hash.make(usuario.nome)

        // Remove caracteres especiais
        const clean_hash = hashed.replace(/[^A-Za-z0-9!?]/g,'');

        // Define o código da sessao
        sessao.codigo = cod + clean_hash;

        // Armazena também o usuário da sessao
        sessao.usuario_id = usuario.id;
        
        // Define a data de expiração da sessão para sete dias após a criação
        let exp = new Date();
        exp.setDate(exp.getDate() + 7);
        
        sessao.expiracao = exp;

        // Salva a sessão na base de dados
        await sessao.save()

        // Retorna a sessão e o id como resposta
        response.send({ usr: sessao.usuario_id, sessao: sessao.codigo });
    }
  }

})

// Cria a rota para validação da sessão
Route.get('api/validacao', async ({ request, response }) => {
  const { usr, sessao } = request.all()

  // Verifica se existe uma sessão com o código definido
  const sessao = await Sessao.findBy('codigo', sessao);

  if(!sessao) {
    response.send({error: "Sessão não encontrada"});
  } else {
    // Verifica se o usuário enviado é o mesmo usuário da sessão, converte os valores para inteiros para facilitar a comparação.
    const usuarioCorreto = parseInt(usr, 10) === parseInt(sessao.usuario_id, 10)

    if(!usuarioCorreto) {
        response.send({error: "Usuário incorreto"});
    } else {
        response.send({ success: "Sessão válida" });
    }
  }

})

```
### Utilizando o Login via React

Em seu projeto React, crie um formulário e uma conexão com a API utilizando Axios:

```
import React from 'react';
import axios from 'axios';

export default function Login() {
 const [error, setError] = useState(false);

 const handleFormSubmit = async (e) => {
   e.preventDefault();
   
   let data = new FormData(e.target);
   
   const response = await axios.post('linkdoservidor.com.br/api/login', data);

   // Caso tenha ocorrido algum erro na solicitação, mostra ao usuario
   if (response.data.error) {
     setError(response.data.error);
   } else {
      // Se não houve erros, armazena no localStorage os dados da sessão
      localStorage.setItem('usr', response.data.usr);
      localStorage.setItem('sessao', response.data.sessao);
   }
 }


 return (
    <div>
      <form>
         <input type="text" name="nome" />
         <input type="text" name="senha" />
         <button type="submit">Login</button>
      </form>
      <div>
        {error}
      </div>
    </div>
 )
}

```

Com o arquivo acima, será possível fazer login com o usuário criado nos passos anteriores. Neste guia, não criaremos redirecionamento para o usuário após o Login, caso esteja utilizando `React Router Dom`, utilize o elemento `<Redirect />` para enviar o usuário para outra página após o login.

Abra as ferramentas para desenvolvedores de seu navegador (`CTRL + SHIFT + I` ou `F12` no Google Chrome), procure a aba `Application` e, nela, você verá os dados que acabou de criar dentro da sessão `Local Storage`.

### Recuperando os dados da sessão

Após o usuário estar conectado, precisamos utilizar essa sessão em outras páginas do React. Para isso, crie outra página com o seguinte código:

```
import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default function Pagina() {
 const [loggedIn, setLoggedIn] = useState(false);

 const getUserLoggedIn = async () => {   
   // Utiliza os dados do localStorage para preencher os campos na URL da verificação
   const response = await axios.get(`linkdoservidor.com.br/api/validacao?usr=${localStorage.getItem('usr')}&sessao=${localStorage.getItem('sessao')}` , data);

   // Caso tenha ocorrido algum erro na solicitação, imprime no console
   if (response.data.error) {
     console.log(response.data.error);
   } else {
      // Se não houve erros, informa o estado que o usuário está logado
      setLoggedIn(true);
   }
 }

  // Roda a função anterior uma única vez, ao montar o componente
  useEffect(() => {
    getUserLoggedIn();
  }, []);


 return (
    <div>
      {loggedIn && <p>Conteúdo protegido liberado</p>}
      {!loggedIn && <p>Conteúdo protegido, faça login para continuar.</p>}
    </div>
 )
}

```

## Conclusão

Pronto! Agora, basta verificar em cada página se o usuário continua logado e a sessão ainda é válida, mantendo assim suas páginas seguras!

Você também pode incluir o código de validação de sessão em suas outras rotas, por exemplo:

usuario/perfil -> Rota que busca os dados pessoais do usuário

Antes de enviar os dados, vocẽ pode verificar se o usuário enviou, junto à requisição, uma sessão válida, e só depois enviar os dados do mesmo, mas isso é parte de outro guia.